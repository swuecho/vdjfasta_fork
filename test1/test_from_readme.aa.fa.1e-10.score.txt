# hmmsearch :: search profile(s) against a sequence database
# HMMER 3.0 (March 2010); http://hmmer.org/
# Copyright (C) 2010 Howard Hughes Medical Institute.
# Freely distributed under the GNU General Public License (GPLv3).
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# query HMM file:                  /home/hwu/app/vdjfasta/db/Vh-linker-Vk.hmm
# target sequence database:        test_from_readme.aa.fa
# MSA of all hits saved to file:   0
# sequence reporting threshold:    E-value <= 1e-10
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Query:       single-chain-alignment  [M=236]
Scores for complete sequences (score includes all domains):
   --- full sequence ---   --- best 1 domain ---    -#dom-
    E-value  score  bias    E-value  score  bias    exp  N  Sequence       Description
    ------- ------ -----    ------- ------ -----   ---- --  --------       -----------
    6.7e-53  169.1   3.4    1.1e-52  168.4   2.0    1.6  2  54_100_frame_0 
    4.9e-45  143.3   2.4    5.5e-45  143.1   0.8    1.7  2  70_100_frame_2 


Domain annotation for each sequence (and alignments):
>> 54_100_frame_0  
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !  168.4   2.0   1.9e-53   1.1e-52       1     113 [.      35     153 ..      35     158 .. 0.95
   2 ?   -2.6   0.0      0.23       1.4      17      38 ..     284     305 ..     281     310 .. 0.85

  Alignments for each domain:
  == domain 1    score: 168.4 bits;  conditional E-value: 1.9e-53
  single-chain-alignment   1 qvqlvesgaelvkpgeslklscaasgftfss.yalsWvrqapgkgLewvglisssgsteYadslkgrvtisrdtskntlylklsslr 86 
                             qvql+esg++lvkp+ +l+l+ca+sg+++ss +++sWvrq+pgkgLew+g i++sgst+Y++slk+rvtis+d+skn+++lklss++
          54_100_frame_0  35 QVQLQESGPGLVKPSGTLSLTCAVSGGSISSsNWWSWVRQPPGKGLEWIGEIYHSGSTNYNPSLKSRVTISVDKSKNQFSLKLSSVT 121
                             79***************************9989****************************************************** PP

  single-chain-alignment  87 aedtavyycar.....eyfdvWGqGtlvtvss 113
                             a+dtavyycar     ++fd+WGqGt+vtvss
          54_100_frame_0 122 AADTAVYYCARklgikYAFDIWGQGTMVTVSS 153
                             *********974444368*************9 PP

  == domain 2    score: -2.6 bits;  conditional E-value: 0.23
  single-chain-alignment  17 slklscaasgftfssyalsWvr 38 
                             + +l c+a+ f+  + ++sW++
          54_100_frame_0 284 KSRLICEATNFSPKQITVSWLQ 305
                             56899999****999*****87 PP

>> 70_100_frame_2  
   #    score  bias  c-Evalue  i-Evalue hmmfrom  hmm to    alifrom  ali to    envfrom  env to     acc
 ---   ------ ----- --------- --------- ------- -------    ------- -------    ------- -------    ----
   1 !  143.1   0.8   9.2e-46   5.5e-45       2     113 ..      31     148 ..      30     153 .. 0.94
   2 ?   -2.1   0.0      0.16      0.98      18      39 ..     280     301 ..     276     335 .. 0.87

  Alignments for each domain:
  == domain 1    score: 143.1 bits;  conditional E-value: 9.2e-46
  single-chain-alignment   2 vqlvesgaelvkpgeslklscaasgftfss..yalsWvrqapgkgLewvglisssgsteYadslkgrvtisrdtskntlylklsslr 86 
                             ++l+esg++lvkp+++l+l+c++sgf++s+  + + W+rq+pgk+Lew++li+++++++Y++slk+r+ti++dts+n+++l++++++
          70_100_frame_2  31 ITLKESGPTLVKPTQTLTLTCTFSGFSLSTsgVGVGWIRQPPGKALEWLALIYWNDDKRYSPSLKSRLTITKDTSENQVVLTMTNMD 117
                             6899************************9977899**************************************************** PP

  single-chain-alignment  87 aedtavyycare....yfdvWGqGtlvtvss 113
                               dta+yyca+     yf++WGqGtlvtvss
          70_100_frame_2 118 PVDTATYYCAHGysygYFQHWGQGTLVTVSS 148
                             *********98523337*************9 PP

  == domain 2    score: -2.1 bits;  conditional E-value: 0.16
  single-chain-alignment  18 lklscaasgftfssyalsWvrq 39 
                              +l c+a+ f+  + ++sW++ 
          70_100_frame_2 280 SRLICEATNFSPKQITVSWLQD 301
                             6889999999999999999875 PP



Internal pipeline statistics summary:
-------------------------------------
Query model(s):                            1  (236 nodes)
Target sequences:                         12  (4962 residues)
Passed MSV filter:                         2  (0.166667); expected 0.2 (0.02)
Passed bias filter:                        2  (0.166667); expected 0.2 (0.02)
Passed Vit filter:                         2  (0.166667); expected 0.0 (0.001)
Passed Fwd filter:                         2  (0.166667); expected 0.0 (1e-05)
Initial search space (Z):                 12  [actual number of targets]
Domain search space  (domZ):               2  [number of targets reported over threshold]
# CPU time: 0.00u 0.01s 00:00:00.01 Elapsed: 00:00:00.02
# Mc/sec: 58.55
//
# Alignment of 2 hits satisfying inclusion thresholds saved to: 0
