#!/usr/bin/perl -w
use v5.14;
use lib "/home/hwu/app/vdjfasta/lib";
use VDJFasta;

# Author:  Jacob Glanville
# Contact: jacob <dot> glanville <at> pfizer <dot> com
#
# Glanville J, Zhai W, Berka J et al. Precise determination of the diversity
# of a combinatorial antibody library gives insight into the human immunoglobulin
# repertoire. Proc Natl Acad Sci USA. 2009;106:20216–20221

############################### Arguments ###############################
# 
use MCE::Loop;
  
MCE::Loop::init {
     max_workers => 12, chunk_size => 1
};
my $dirname = '/home/hwu/analysis/82-2015-04-10_57414a_Ig_Repertoire_118_089';

my @files = glob "$dirname/fasta_splited_without_cdr3/*.fa";

my $total = @files;
my $t = time;
say "start at: $t. total files # $total";
mce_loop {
         my ($mce, $chunk_ref, $chunk_id) = @_;
         say $_;
          analysis_fasta($_);
} @files;
my $end_t = time;
say "end time: $end_t";
my $total_time = ($end_t- $t);
say "total time used: $total_time";

sub analysis_fasta {
    my ($seqfile) = @_;
    my $verbose = 0;
    my ( $vdb, $ddb, $jdb, $cdb );

    my $corename = stripFastaSuffix($seqfile);

############################### Inputs    ###############################

    my $fasta = VDJFasta->new();

    $fasta->loadSeqs($seqfile);

############################### Analysis  ###############################

    # germline classification
    if ($verbose) { print "Running constant domain classification...\n"; }

    $fasta->getGermlines( "C", "$corename.C.germdata.txt", "dna", $cdb );

    if ($verbose) { print "Running J-segment classification...\n"; }
    $fasta->getGermlines( "J", "$corename.J.germdata.txt", "dna", $jdb );

    if ($verbose) { print "Running V-segment classification...\n"; }
    $fasta->getGermlines( "V", "$corename.V.germdata.txt", "dna", $vdb );

    if ($verbose) { print "Running D-segment classification...\n"; }
    $fasta->getGermlines( "D", "$corename.D.germdata.txt", "dna", $ddb );
    $fasta->writeDsegCoords("$corename.VDJ.coords.txt");

    if ($verbose) { print "Translating all frames...\n"; }
    $fasta->translateAllFramesToFile("$corename.aa.fa");

    my $aafasta = VDJFasta->new();
    $aafasta->loadSeqs("$corename.aa.fa");
    if ($verbose) {
        print "Scoring aa sequences for immunoglobulin content...\n";
    }

    my @scoredHits = $aafasta->igScore();

    if ($verbose) { print "Extracting ig-bearing subset...\n"; }

    $aafasta->printSeqSubsetbyList( \@scoredHits, "$corename.wIgs.fa" );

    undef($aafasta);

    my $igfasta = VDJFasta->new();
    $igfasta->loadSeqs("$corename.wIgs.fa");
    if ($verbose) { print "Aligning all sequences to VDJ HMM...\n"; }
    my $c2m      = $igfasta->aascFvc2m();
    my $c2mfasta = VDJFasta->new();
    $c2mfasta->loadSeqs($c2m);
    if ($verbose) {
        print
          "Identifying translated CDR-H3 with VDJ HMM-boundary QC motifs...\n";
    }
    $c2mfasta->qcH3("$corename.H3.acc.txt");
    if ($verbose) {
        print
          "Identifying translated CDR-L3 with VDJ HMM-boundary QC motifs...\n";
    }
    $c2mfasta->qcL3("$corename.L3.acc.txt");
    if ($verbose) {
        print
          "Identifying translated CDR-H1 with VDJ HMM-boundary QC motifs...\n";
    }
    $c2mfasta->qcH1("$corename.H1.acc.txt");
    if ($verbose) {
        print
          "Identifying translated CDR-H2 with VDJ HMM-boundary QC motifs...\n";
    }
    $c2mfasta->qcH2("$corename.H2.acc.txt");
    if ($verbose) {
        print
          "Identifying translated CDR-L1 with VDJ HMM-boundary QC motifs...\n";
    }
    $c2mfasta->qcL1("$corename.L1.acc.txt");
    if ($verbose) {
        print
          "Identifying translated CDR-L2 with VDJ HMM-boundary QC motifs...\n";
    }
    $c2mfasta->qcL2("$corename.L2.acc.txt");

    if ($verbose) {
        print "Add all annotations to $corename.aa.VDJ.H3.L3.CH1.fa...\n";
    }
    $igfasta->addHeaderAnnotation("$corename.V.germdata.txt");
    $igfasta->addHeaderAnnotation("$corename.D.germdata.txt");
    $igfasta->addHeaderAnnotation("$corename.J.germdata.txt");
    $igfasta->addHeaderAnnotation("$corename.H3.acc.txt");
    $igfasta->addHeaderAnnotation("$corename.L3.acc.txt");
    $igfasta->addHeaderAnnotation("$corename.C.germdata.txt");
    $igfasta->addHeaderAnnotation("$corename.VDJ.coords.txt");
    $igfasta->addHeaderAnnotation("$corename.H1.acc.txt");
    $igfasta->addHeaderAnnotation("$corename.H2.acc.txt");
    $igfasta->addHeaderAnnotation("$corename.L1.acc.txt");
    $igfasta->addHeaderAnnotation("$corename.L2.acc.txt");
    $igfasta->writeSeqs("$corename.aa.VDJ.H3.L3.CH1.fa");
}

sub stripFastaSuffix {
     my $corename = shift;
     $corename =~s/fasta_splited_without_cdr3/vdjfasta/;
     return $corename =~ s/.fa$//r;
}

